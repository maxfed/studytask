#include <math.h>
#include <stdlib.h>
#include <stdio.h>
#include "mpi.h"
#include <mpi-ext.h>
#include <signal.h>
#include <string.h>

#define Max(a,b) ((a)>(b)?(a):(b))
#define N (1024)
#define PROCESS_TO_KILL 1
#define ITER_TO_KILL 3
#define ADD_PROCESS 1

double maxeps = 0.1e-5;
int itmax = 20;
int i,j;
int count_op,shift;
double eps;

double (*A)[N];
double * buf;
int buf_size = 10*N;

int myrank, ranksize, full_ranksize;
int startrow,lastrow,nrow;
MPI_Status status[4];
MPI_Comm comm = MPI_COMM_WORLD;
char filename[50];
int cnt_error = 0;

int count_digit();
void itoa();
void update_filename();
void relax();
void init();
void verify();
void save_backup();
void load_backup();

static void err_handler(MPI_Comm *pcomm, int *perr, ...) {
    int err = *perr;
    char errstr[MPI_MAX_ERROR_STRING];
    char rank_str[20];
    int nf, len, old_rank=myrank;
    MPI_Group group_f;

    MPIX_Comm_failure_ack(comm);
    MPIX_Comm_failure_get_acked(comm, &group_f);
    MPI_Group_size(group_f, &nf);
    MPI_Error_string(err, errstr, &len);
	
	MPIX_Comm_shrink(comm, &comm);
    MPI_Comm_rank(comm, &myrank);
    MPI_Comm_size(comm, &full_ranksize);
    //printf("\nRank(new) %d(%d) / %d: Notified of error %s. %d found dead\n", old_rank, myrank, full_ranksize, errstr, nf);
    startrow = (myrank*N)/ranksize;
	lastrow=((myrank+1)*N)/ranksize-1;
	nrow = lastrow-startrow+1;
    update_filename(filename, myrank);
}

int main(int argc, char *argv[]){
	int it, need_load_backup = 1;
	int do_sigkill = 1;
	buf = malloc(buf_size);
	MPI_Init(&argc,&argv);
	MPI_Comm_rank(comm,&myrank);
	MPI_Comm_size(comm,&full_ranksize);

	ranksize = full_ranksize - ADD_PROCESS;
	update_filename(filename, myrank);

	MPI_Errhandler errhandl;
    MPI_Comm_create_errhandler(err_handler, &errhandl);
    MPI_Comm_set_errhandler(comm, errhandl);

	MPI_Buffer_attach(buf,buf_size);
	MPI_Barrier(comm);
	startrow = (myrank*N)/ranksize;
	lastrow=((myrank+1)*N)/ranksize-1;
	nrow = lastrow-startrow+1;
	double time = MPI_Wtime();
	A = malloc((nrow+2)*N*sizeof(double));
	if (myrank < ranksize){
		init();
		save_backup();
	}
	MPI_Barrier(comm);

	if (myrank == PROCESS_TO_KILL){
		raise(SIGKILL);
	}
	for(it=1; it<=itmax; it++){
		MPI_Barrier(comm);
		BackupPoint:
		if (need_load_backup){
			need_load_backup = 0;
			if (myrank < ranksize)
				load_backup();
		}
		eps = 0.;
		relax();
		if (cnt_error > 0){
			cnt_error = 0;
			need_load_backup = 1;
			goto BackupPoint;
		}
		MPI_Barrier(comm);
		if (myrank < ranksize)
			save_backup();
		if (myrank==0) printf( "it=%4i eps=%f\n", it,eps);
		if (eps < maxeps) break;
	}
	if (myrank < ranksize){
		verify();
	}
	free(A);
	if (myrank==0) printf("time = %f, ranksize = %d\n", MPI_Wtime()-time, ranksize);

	MPI_Finalize();
	free(buf);
	return 0;
}

void init(){
	for (i=0; i<=nrow+1; ++i)
		for (j=0; j<=N-1; ++j){
			if (i==0 || i==nrow+1 || i==1 && myrank==0 || i==nrow && myrank==ranksize-1 || j==0 || j==N - 1)
				A[i][j]= 0.;
			else 
				A[i][j]= (1. + startrow + i + j - 1.) ; // -1 т.к. самая первая строчка - фиктивная
		}
}

void relax(){
	if (myrank < ranksize){
		MPI_Status st;
		// получаем крайние строчки массива с "соседних" процессов
		if (myrank!=0)
			MPI_Send(&A[1][0],N,MPI_DOUBLE,myrank-1,2,comm);
		if (myrank != ranksize - 1)
			MPI_Recv(&A[nrow + 1][0], N, MPI_DOUBLE, myrank + 1, 2, comm, &st);
		for (j = 1; j <= N - 2; j++){
			if (myrank != 0)
				MPI_Recv(&A[0][j], 1, MPI_DOUBLE, myrank - 1, 3, comm, &st);
			for (i = 1; i <= nrow; i++) {
				if (((i == 1) && (myrank == 0)) || ((i == nrow) && (myrank == ranksize - 1)))
					continue;
				double e;
				e = A[i][j];
				A[i][j]=(A[i-1][j]+A[i+1][j]+A[i][j-1]+A[i][j+1])/4.;
				eps = Max(eps, fabs(e - A[i][j]));
			}
			if (myrank != ranksize - 1) {
				MPI_Send(&A[nrow][j], 1, MPI_DOUBLE, myrank + 1, 3, comm);
			}
		}
		//синхронизируем eps, ищем максимальный по всем потокам

		if (myrank==0)
			for (i=1; i<ranksize; i++){
				double tmp;
				MPI_Recv(&tmp,1,MPI_DOUBLE,MPI_ANY_SOURCE,3,comm,&status[1]);
				eps = Max(eps,tmp);
			}
		if (myrank!=0)
			MPI_Ssend(&eps,1,MPI_DOUBLE,0,3,comm);
	}
	if (myrank!=0)
		MPI_Recv(&eps,1,MPI_DOUBLE,0,4,comm,&status[1]);
	if (myrank==0){
		for (i=1; i<full_ranksize; i++)
			MPI_Ssend(&eps,1,MPI_DOUBLE,i,4,comm);
	}
}

void verify(){
	double s;
	s = 0.;
	for(i=1; i<=nrow; ++i)
		for(j=0; j<=N-1; ++j){
			s=s+A[i][j]*((i-1)+1+startrow)*(j+1)/(N*N); // -1 т.к. добавили фейковую нулевую строку
		}
	if (myrank==0 && ranksize>1)
		for (i=1; i<ranksize; ++i){
			double tmp;
			MPI_Recv(&tmp,1,MPI_DOUBLE,i,5,comm,&status[1]);
			s += tmp;
		}
	if (myrank!=0)
		MPI_Ssend(&s,1,MPI_DOUBLE,0,5,comm);

	if (myrank==0) printf(" S = %f\n",s);
}

int count_digit(int x){
  if (x < 0)
    printf("Error! count_digit(): x < 0 => Undefined behaviour\n");
  if (x == 0)
    return 1;
  int n = 1;
  while ((x /= 10) > 0)
    ++n;
  return n;
}

void itoa(int x, char* str){
  if (x == 0){
    str[0] = '0';
    str[1] = '\0';
    return;
  }
  int i, n;
  n = count_digit(x);
  i = n - 1;
  while (x > 0){
    str[i] = '0' + x % 10;
    --i;
    x /= 10;
  }
  str[n] = '\0';
  return;
}

void update_filename(char* str, int myrank){
    itoa(myrank, str);
    strcat(str, ".backup");
    return;
}

void save_backup(){
    FILE *fp = fopen(filename, "w");
    for (int i = 0; i < nrow + 1; ++i)
    	for (int j = 0; j < N; ++j)
        	fprintf(fp, "%lf ", A[i][j]);
    fclose(fp);
}

void load_backup(){
    FILE *fp = fopen(filename, "r");
    for (int i = 0; i < nrow + 1; ++i)
    	for (int j = 0; j < N; ++j)
        	fscanf(fp, "%lf", &A[i][j]);    
    fclose(fp);
}
