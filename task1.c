#include <mpi.h>
#include <stdio.h>

#define L 100
#define SIZE 5
#define NDIMS 2

int main(int argc, char **argv){
    char buf[L/2];
    int rank, i;
    int rank_from = -1, rank_to;
    int dims[2] = {SIZE, SIZE};
    int periods[2] = {0, 0};
    MPI_Init(&argc, &argv);

    MPI_Comm comm_cart;
    MPI_Cart_create(MPI_COMM_WORLD, NDIMS, dims, periods, 0, &comm_cart);
    MPI_Comm_rank(comm_cart, &rank);
    int rank_coords[2];
    MPI_Cart_coords(comm_cart, rank, 2, rank_coords);

    if (rank == 0){
      char message[L] = "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore";
      MPI_Request req1, req2;
      printf("Send msg = %s\n", message);
      MPI_Issend(message, L/2, MPI_CHAR, 1, 1, MPI_COMM_WORLD, &req1);
      MPI_Issend(&message[L/2], L/2, MPI_CHAR, SIZE, 2, MPI_COMM_WORLD, &req2);
    }
    else if (rank == SIZE * SIZE - 1){
      MPI_Request req1, req2;
      char message[L];
      MPI_Irecv(message, L/2, MPI_CHAR, rank - SIZE, 1, MPI_COMM_WORLD, &req1);
      MPI_Irecv(&message[L/2], L/2, MPI_CHAR, rank - 1, 2, MPI_COMM_WORLD, &req2);
      MPI_Wait(&req1,  MPI_STATUS_IGNORE);
      MPI_Wait(&req2,  MPI_STATUS_IGNORE);
      printf("Received msg = %s\n", message);
    }
    else if (rank_coords[0] == 0){
      rank_from = rank - 1;
      rank_to = rank == SIZE - 1 ? rank + SIZE : rank + 1;
    }
    else if (rank_coords[1] == SIZE - 1){
      rank_from = rank - SIZE;
      rank_to = rank + SIZE;
    }
    else if (rank_coords[1] == 0){
      rank_from = rank - SIZE;
      rank_to = rank == SIZE * (SIZE - 1) ? rank + 1 : rank + SIZE;
    }
    else if (rank_coords[0] == SIZE - 1){
      rank_from = rank - 1;
      rank_to = rank + 1;
    }
    
    if (rank_from != -1){
      MPI_Status st;
      MPI_Recv(buf, L/2, MPI_CHAR, rank_from, MPI_ANY_TAG, MPI_COMM_WORLD, &st);
      int tag = st.MPI_TAG;
      MPI_Ssend(buf, L/2, MPI_CHAR, rank_to, tag, MPI_COMM_WORLD);
    }
    
    MPI_Barrier(MPI_COMM_WORLD);
    MPI_Finalize();
    return 0;
}
